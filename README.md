# Information – dagens EPUB

_This is a script to generate an EPUB file of today's issue of the Danish daily Information. As the paper is in Danish, the following README will also be in Danish._

[Dagbladet Information](https://www.information.dk/) udkommer i mange formater: På papir, som app til smartphones/tablets og [online på web](https://www.information.dk/dagensavis). Men et oplagt format har manglet: EPUB til din e-boglæser.

For at udfylde formathullet, og gøre den mindst ringe avis [lidt mere mindst](https://www.youtube.com/watch?v=9ar0TF7J5f0) ringe, har jeg lavet dette script.

## Hvordan kommer jeg i gang?

Du skal have Python 3 på din computer. Hvis det ikke siger dig noget kan du nok lige så godt give op nu, medmindre du har en edb-kyndig mormor der kan hjælpe dig.

Den nemmeste måde er at [installere fra PyPI](https://pypi.org/project/informeren-dagens-epub/): `pip install informeren-dagens-epub`.

Alternativt kan du klone koden direkte her fra GitLab: `git clone git@gitlab.com:gorgonzola/informeren-dagens-epub.git` og kør herefter `pip install -r requirements.txt` for at installere tredjepartsafhængigheder.

Uanset installationsmetode er det en god ide at installere i et isoleret Python-miljø, fx via [`virtualenv`](https://virtualenv.pypa.io/en/stable/) eller lignende.

## Hvordan får jeg min EPUB-fil?

Når alt er installeret kan du køre kommandoen `informeren-epub -h` for at få følgende hjælp vist (på engelsk):

```
$ informeren-epub -h
Informeren – Today's EPUB

Usage: informeren-epub [options]

Available options are:

-f, --firefox-cookie-path <path>    Path to the `cookies.sqlite` file in your
                                    Firefox profile.
-j, --json-cookie-path <path>       Path to a JSON cookie file (e.g. generated
                                    with the combination of the `-f` and
                                    `-P flags.).
-d, --date <dd-mm-yyyy>             Date of the newspaper issue to get an EPUB
                                    for. The format may seem a bit odd, but it
                                    follows the format of the paths for the web
                                    version of the newspaper.
-P, --print-cookies                 Print the loaded cookies instead of
                                    generating the EPUB file. This is useful
                                    if you want to generate a JSON cookie file
                                    that can be exported to another computer
                                    without a Firefox that is logged in to
                                    information.dk with an active subscription.
-h, --help                          Show this message.
```

EPUB'en bliver genereret på baggrund af [dagens avis (evt. arkivet) fra information.dk](https://www.information.dk/dagensavis). Da mange af artiklerne er låst inde bag Informations betalingsmur kræver det:

 1. At du har abonnement på Information med online adgang.
 2. At du i Firefox er logget ind på information.dk med en bruger tilknyttet dit abonnement.

Scriptet låner dine cookies fra Firefox[1] for at komme ind bag betalingsmuren. Så for at få dagens avis som EPUB skal du køre følgende kommando (men rette stien til din Firefox-profil til):

`informeren-epub -f ~/.mozilla/firefox/your.profile/cookies.sqlite`

Dette vil generere en fil med et navn a la `<dd-mm-yyyy>.epub` i den mappe du står i. Ønsker du avisen fra en bestemt dag, kan den trækkes ud ved hjælp af argumentet `-d`. Fx kan en EPUB for avisen fra 6. maj 2015 genereres med `-d 06-05-2015` og vil resultere i filen `06-05-2015.epub`.

## Hvordan får jeg min EPUB-fil over på min e-bogslæser?  

Det er et rigtig godt spørgsmål. Der er mange forskellige modeller fra forskellige producenter som virker forskelligt. Til de fleste e-bogslæsere kan man overføre filen via et USB-kabel. Selv har jeg en fra Kobo med wifi og indbygget browser. Så jeg har lavet mig selv et ekstra lille script der genererer EPUB'en og kopierer den over til en webserver, hvorfra min Kobo kan hente EPUB'en inden jeg sletter den igen. Find selv på en metode der fungerer for dig.

## Det lyder røvsvært!

Ja, det kræver lidt teknisk snilde. Jeg ville hjertens gerne stille en menneskevenlig tjeneste til rådighed, man da anvendelse af scriptet kun er lovligt så længe man som abonnent benytter det til sig selv ([ophavsrettens §12](https://da.wikisource.org/wiki/Lov_om_ophavsret) om eksemplarfremstilling til privat brug), vil det være op til den enkelte abonnent selv at fremstille EPUB'en (eksemplaret) til eget brug.

## Hvorfor kan man ikke X? Og hvorfor virker Y ikke?

Har du ideer til forbedringer eller problemer med at komme i gang, er du velkommen til at kontakte mig [via e-mail](mailto:3xm@detfalskested.dk), [på Twitter](https://twitter.com/decibyte), [åbne et issue](https://gitlab.com/gorgonzola/informeren-dagens-epub/issues) eller aller bedst: [Komme med et merge request](https://gitlab.com/gorgonzola/informeren-dagens-epub/merge_requests) med rettelser eller ny funktionalitet.

## Hvad med opdateringer?

Jeg retter løbende det visuelle i EPUB'en til. Der er nogle elementer der fungerer på web som ikke rigtigt fungerer i en e-bogslæser. Og dem justerer jeg efterhånden som jeg selv falder over dem. Peg dem gerne ud hvis du falder over noget.

Derfor er det også en god ide at sørge for at holde din version opdateret. Det kan gøres med `pip install --upgrade informeren-dagens-epub` eller `git pull && pip install -r requirements.txt`, afhængigt af hvordan du har installeret. En ting på min todo er at lave et indbygget versionstjek der kan minde dig om at der er en ny version tilgængelig.

## Noter

[1] Det kunne være lækkert også at understøtte andre browsere, men fx Chrom(e|ium) krypterer sine cookies og gør det lidt sværere at trække dem ud. Det kan lade sig gøre af dekryptere dem, men da jeg selv bruger Firefox, vil jeg lade det være op til brugere af andre browsere at implementere understøttelse for disse (merge requests er meget velkomne!).

## Tak til

Christoffer Buchholz for, med sine fejlrapporter, at hjælpe med til at stabilisere koden.
