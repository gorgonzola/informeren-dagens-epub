# Changelog for `informeren-dagens-epub`


## 1.0.2 (2018-08-28)

Fixes a bug: Don't break the extraction of the table of contents if an article
is missing a description.

## 1.0.1 (2018-08-23)

A few bugfixes:

 * Reference the logger properly when a front page image is not found.
 * Make sure all template files are proper UTF-8 with Unix line terminators.

## 1.0.0 (2018-05-23)

First public release, featuring:

 * EPUB extraction of any issue available from [the archive at information.dk](https://www.information.dk/dagensavis)
   (defaults to most recent issue).
 * Support for cookie extraction from Firefox or a JSON cookie file.
 * Support for printing relevant cookies in order to create a JSON cookie file.
